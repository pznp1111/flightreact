
import React from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { requestApiData } from "./actions";
import DataTable from "react-data-table-component";
import Card from "@material-ui/core/Card";
import Checkbox from "@material-ui/core/Checkbox";
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Button from '@material-ui/core/Button';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const columns = [
  {
    name: "departure",
    selector: "departure"
  },
  {
    name: "arrival",
    selector: "arrival"
  },
  {
    name: "class",
    selector: "class"
  },
  {
    name: "departureTime",
    selector: "departureTimeFormatted"
  },
  {
    name: "arrivalTime",
    selector: "arrivalTimeFormatted"
  }
];

const isIndeterminate = indeterminate => indeterminate;
const selectableRowsComponentProps = { indeterminate: isIndeterminate };
toast.configure()

class Home extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      departure: '',
      arrival:'',
      class:'',
      filteredData:[]
    };

    this.handleChangeDeparture = this.handleChangeDeparture.bind(this);
    this.handleChangeArrival = this.handleChangeArrival.bind(this);
    this.handleChangeClass = this.handleChangeClass.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChangeDeparture(event) {
      this.setState({departure: event.target.value});
  }
  handleChangeArrival(event) {
      this.setState({arrival: event.target.value});
  }
  handleChangeClass(event) {
      this.setState({class: event.target.value});
  }

  handleSubmit(event) {
    var departure = this.state.departure;
    var arrival = this.state.arrival;
    var classtype = this.state.class;
    event.preventDefault();
    if(arrival !== ""){
      if(arrival === departure  ){
        toast("Please choose different Departure and Arrival");
      }
    } else{
      if(arrival === departure){
        toast("Departure and Arrival cannot be empty");
      }
    }



    var filteredData = this.props.data[0];
    if(classtype !== ""){
        filteredData = this.props.data[0].filter(function (item) {
        if(item["class"]=== classtype) {
            return true;
        } else{
            return false;
        }
        });
    }

    if(departure !== ""  && arrival !== ""){
        filteredData = filteredData.filter(function (item) {
        if(item["departure"]=== departure  && item["arrival"] === arrival) {
            return true;
        } else{
            return false;
        }
        });

      //  console.log("participantStart",filteredData);
        this.setState({filteredData:filteredData});
    }
    if(departure === ""  && arrival !== ""){
        filteredData = filteredData.filter(function (item) {
        if(item["arrival"] === arrival) {
            return true;
        } else{
            return false;
        }
        });
        console.log("participantStart",filteredData);
        this.setState({filteredData:filteredData});
    }
    if(departure !== ""  && arrival === ""){
        filteredData = filteredData.filter(function (item) {
        if(item["departure"]=== departure  ) {
            return true;
        } else{
            return false;
        }
        });
        this.setState({filteredData:filteredData});
    }
  }

  componentDidMount() {
    //console.log = console.warn = console.error = () => {};
    this.props.requestApiData();
  }

  render() {
    return  this.props.data!== undefined && this.props.data.length
      ?
      <div className="App">
        <div className="Age-label">
          Search Flight
        </div>

        <div>
          <form onSubmit={this.handleSubmit}>
          <FormControl style={{margin: 20,minWidth: 100}}>
            <InputLabel>Class:</InputLabel>
            <Select value={this.state.class} onChange={this.handleChangeClass}>
              <MenuItem value=""></MenuItem>
              <MenuItem value="cheap">cheap</MenuItem>
              <MenuItem value="business">business</MenuItem>
            </Select>
          </FormControl>

          <FormControl style={{margin: 20,minWidth: 100}}>
            <InputLabel>Departure:</InputLabel>
            <Select options={this.state.departure||''} onChange={this.handleChangeDeparture}>
            <MenuItem value=""></MenuItem>
              {this.props.data[5].map((team) => <MenuItem key={team} value={team}>{team}</MenuItem>)}
            </Select>
          </FormControl>

          <FormControl style={{margin: 20,minWidth: 100}}>
            <InputLabel>Arrival:</InputLabel>
            <Select value={this.state.arrival||''} onChange={this.handleChangeArrival}>
            <MenuItem value=""></MenuItem>
              {this.props.data[6].map((team) => <MenuItem key={team} value={team}>{team}</MenuItem>)}
            </Select>
          </FormControl>
          <FormControl style={{margin: 20,minWidth: 40}}>

          </FormControl>
          <FormControl style={{margin: 20,minWidth: 40}}>
          <Button variant="contained" type="submit" value="Submit">Search</Button>
          </FormControl>
        </form>
        </div>

        <div>
        <div className="App">
          <Card>
            <DataTable
              title="Flights"
              columns={columns}
              data={this.state.filteredData}
              defaultSortField="title"
              pagination
              selectableRows
              selectableRowsComponent={Checkbox}
              selectableRowsComponentProps={selectableRowsComponentProps}
            />
          </Card>
        </div>
        </div>
      </div>

      : <h1>loading...</h1>;
  }
}

const mapStateToProps = state => ({ data: state.data });

const mapDispatchToProps = dispatch =>
  bindActionCreators({ requestApiData }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Home);
