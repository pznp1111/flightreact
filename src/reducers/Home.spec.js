import React from 'react';
import axios from 'axios';
import renderer from 'react-test-renderer';
import { configure,shallow, mount} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import ReactDOM from 'react-dom';
import{Provider} from 'react-redux';
import configureStore from "redux-mock-store";
import {requestApiData} from "../actions";
import {runSaga} from 'redux-saga';
import {fetchData} from '../api';

configure({ adapter: new Adapter() });

import Home from '../Home';
import getApiData from '../sagas'

const resolvePromise = () => Promise.resolve();
const rejectPromise = () => Promise.reject("Error");
const mockStore = configureStore();
const store = mockStore({});

test('fetchData API',async()=>{
  var result = await fetchData().then(all =>{
    expect(all.length).toBe(7);
  });
});

test('snapshot renders', () => {
const wrapper = mount(
      <Provider store= {store}>
          <Home />
       </Provider>
  );
  expect(wrapper).toMatchSnapshot();
});

test('componentdidmount', () => {
  const wrapper = mount(
        <Provider store= {store}>
            <Home
            requestApiData ={resolvePromise}
            />
         </Provider>
    );
    return Promise
    .resolve(wrapper)
    .then(() => {})
    .then(() => {
      wrapper.update()
      expect(wrapper.text()).toContain("loading");
    });

});
